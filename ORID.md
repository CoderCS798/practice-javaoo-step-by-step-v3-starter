## O:

Today I learned about object-oriented programming and Java Stream

## R：

I feel it is useful and a little difficult

## I：

I think the Stream API such as filter, map and reduce methods learned today can make the code more concise and clear, object-oriented programming by the same type of method encapsulation, so that it can realize the inheritance and polymorphism and other operations to improve the reusability of the code.

## D:

The idea of object-oriented programming is well worth learning, and it is important to maximize the reusability of code when programming