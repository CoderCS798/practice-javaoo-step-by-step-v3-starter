package ooss;

public class Client {
    public static void main(String[] args) {
        Student tom = new Student(1, "Tom", 18);
        Klass klass = new Klass(1);
        tom.join(klass);

        String introduce = tom.introduce();
        System.out.println(introduce);
    }

}
