package ooss;

public class Student extends Person {
    private int number;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override

    public String introduce() {
        return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.",this.name,this.age,number);
    }
    public void join(Klass klass) {
        number = klass.number;

    }

    public boolean isIn(Klass klass) {
        if(number == klass.number) {
            return true;
        }
        return false;
    }
}
