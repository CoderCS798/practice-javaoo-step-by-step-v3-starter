package ooss;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private Set<Klass> klasses = new HashSet<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);

    }

    @Override
    public String introduce() {
        String klassList = this.klasses.stream()
                .map(k -> "" + k.getNumber())
                .collect(Collectors.joining(", "));


        if(klasses.size() == 0){
            return String.format("My name is %s. I am %d years old. I am a teacher.",this.name,this.age);
        }
        return String.format("My name is %s. I am %d years old. I am a teacher. I teach Class %s.", name, age,klassList);
    }

    public void assignTo(Klass klass) {
        klasses.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        if(klasses.contains(klass)){
            return true;
        }
        return false;
    }

    public boolean isTeaching(Student tom) {

        return false;
    }
}
